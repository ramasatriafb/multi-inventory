<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>MULTI INVENTORY</title>

  <link rel="stylesheet" href="/css/app.css">
  <link rel="shortcut icon" type="image/x-icon" href="./img/inventory.png" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper" id="app">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- SEARCH FORM -->
    <div class="input-group input-group-sm">
      <input class="form-control form-control-navbar" @keyup="searchit" v-model="search" type="search" placeholder="Cari" aria-label="Search">
      <div class="input-group-append">
        <button class="btn btn-navbar" @click="searchit">
          <i class="fas fa-search"></i>
        </button>
      </div>
    </div>
    
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="./img/inventory.png" alt="Start UP Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Multi Inventory</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="./img/profile/{{ Auth::user()->photo }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">
            {{ Auth::user()->name }}
            <p>{{ Auth::user()->type }}</p>
          </a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <router-link to="/dashboard" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt blue"></i>
              <p>
                Dashboard
              </p>
            </router-link>
          </li>
          
          <li class="nav-item">
            <router-link to="/barang" class="nav-link">
              <i class="nav-icon fas fa-database green"></i>
              <p>
                Data Barang
              </p>
            </router-link>
          </li>
        
          @can('isAdmin')
          <li class="nav-item treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-cog green"></i>
              <p>
                Manajemen Sistem
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <router-link to="/users" class="nav-link">
                  <i class="fas fa-users nav-icon"></i>
                  <p>Pengguna Sistem</p>
                </router-link>
              </li>
              <li class="nav-item">
                <router-link to="/kategori" class="nav-link">
                  <i class="fas fa-book-open nav-icon"></i>
                  <p>Catatan Log Sistem</p>
                </router-link>
              </li>
              <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                  <i class="far fas fa-database red"></i>
                  <p>
                    Restore Data
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <router-link to="/restorebarang" class="nav-link">
                      <i class="nav-icon fas fa-database"></i>
                      <p>Data Barang</p>
                    </router-link>
                  </li>
                </ul>
              </li> 
            </ul>
          </li>

          <li class="nav-item treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-bookmark info"></i>
              <p>
                Referensi
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <router-link to="/kategori" class="nav-link">
                  <i class="fas fa-list nav-icon"></i>
                  <p>Kategori Inventaris</p>
                </router-link>
                <li class="nav-item">
                  <router-link to="/jenis" class="nav-link">
                    <i class="fas fa-list-alt nav-icon"></i>
                    <p>Jenis Barang</p>
                  </router-link>
                </li>
                <li class="nav-item">
                  <router-link to="/Proyek" class="nav-link">
                    <i class="fas fa-tasks nav-icon"></i>
                    <p>Data Proyek</p>
                  </router-link>
                </li>  
              </li>
            </ul>
          </li>

          <li class="nav-item treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fab fa-wpforms"></i>
              <p>
                Form Inventaris
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <router-link to="/peminjaman" class="nav-link">
                  <i class="fas far fa-circle  nav-icon"></i>
                  <p>Form Peminjaman</p>
                </router-link>
                <li class="nav-item">
                  <router-link to="/jenis" class="nav-link">
                    <i class="fas far fa-circle nav-icon"></i>
                    <p>Form Pengembalian</p>
                  </router-link>
                </li>
                <li class="nav-item">
                  <router-link to="/Proyek" class="nav-link">
                    <i class="fas far fa-circle  nav-icon"></i>
                    <p>Form Perbaikan</p>
                  </router-link>
                </li>  
              </li>
            </ul>
          </li>

          <li class="nav-item treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas far fa-book"></i>
              <p>
                Rekap Inventaris
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <router-link to="/rekappeminjaman" class="nav-link">
                  <i class="fas far fa-circle  nav-icon"></i>
                  <p>Rekap Peminjaman</p>
                </router-link>
                <li class="nav-item">
                  <router-link to="/jenis" class="nav-link">
                    <i class="fas far fa-circle nav-icon"></i>
                    <p>Rekap Pengembalian</p>
                  </router-link>
                </li>
                <li class="nav-item">
                  <router-link to="/Proyek" class="nav-link">
                    <i class="fas far fa-circle  nav-icon"></i>
                    <p>Rekap Perbaikan</p>
                  </router-link>
                </li>  
              </li>
            </ul>
          </li>

          <li class="nav-item">
            <router-link to="/developer" class="nav-link">
              <i class="nav-icon fas fa-cogs"></i>
              <p>
                Developer
              </p>
            </router-link>
          </li>
           @endcan
          <li class="nav-item">
            <router-link to="/profile" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Profil
              </p>
            </router-link>
          </li>
        
          <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}"
              onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                     
              <i class="nav-icon fas fa-sign-out-alt red"></i>  
              <p>     
              {{ __('Keluar Sistem') }}
              </p>
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
          </form>

          </li>
        
          </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <div class="content">
     <router-view></router-view>
     
     <vue-progress-bar></vue-progress-bar>
      <div class="container-fluid">
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2014-2019 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

@auth
<script>
  window.user = @json(auth()->user())
</script>
@endauth
<script src="/js/app.js"></script>
</body>
</html>
