<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proyek extends Model
{
    protected $table = 'proyek';

    protected $fillable = [
        'nama_proyek',
        'lokasi',
        'penanggung_jawab'
    ];
}
