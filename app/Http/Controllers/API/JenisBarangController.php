<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\JenisBarang;

class JenisBarangController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\Gate::allows('isAdmin') || \ Gate::allows('isAuthor')){
            return JenisBarang::latest()->paginate(5);
        }
    }

    public function getDataJenisBarang()
    {
            return JenisBarang::all();
        
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'jenis' => 'required|string|max:191'
        ]);

        return JenisBarang::create([
            'jenis' => $request['jenis'],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $type = JenisBarang::findOrFail($id);

        $this->validate($request,[
            'jenis' => 'required|string|max:191'
        ]);

        $type->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('isAdmin');
        $type = JenisBarang::findOrFail($id);

        //delete user
        $type->delete();

        return ['message' => ' Jenis Barang dihapus'];
    }

    public function search(){
        if ($search = \Request::get('q')){
            $types = JenisBarang::where(function($query) use ($search){
                $query->where('jenis','LIKE', "%$search%");
            })->paginate(5);
        }else{
            $types = JenisBarang::latest()->paginate(5);
        }
        return $types;
    }
}
