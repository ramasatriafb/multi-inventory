<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Barang;
use Illuminate\Support\Facades\DB;


class BarangController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\Gate::allows('isAdmin') || \ Gate::allows('isAuthor')){
            $items = DB::table('barang')->select('barang.kode_barang', 'barang.nama_barang', 'kategori_inventaris.kategori'
            , 'kategori_inventaris.id AS kategori_id', 'jenis_barang.jenis','jenis_barang.id as jenisbarang_id','barang.kelengkapan'
            ,'barang.foto_barang','barang.file_invoice','barang.status' ,'barang.merk','barang.dipinjam','barang.keterangan', 'barang.created_at')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            )->whereNull('deleted_at')->orderBy('barang.created_at','desc')->paginate(15);
            return $items;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user =  auth('api')->user();

        $this->validate($request,[
            'kode_barang' => 'required|string|max:191|unique:barang',
            'nama_barang' => 'required|string|max:191',
            'foto_barang' => 'required',
            'file_invoice' => 'required'
        ]);

        $items = new Barang;
        $items->kode_barang = $request->get('kode_barang');
        $items->nama_barang = $request->get('nama_barang');
        $items->merk = $request->get('merk');
        $items->kelengkapan = $request->get('kelengkapan');
        $items->status = $request->get('status');
        $foto_barang = $request->get('foto_barang');
        if($foto_barang){
            $foto = time().'.' . explode('/', explode(':', substr($request->foto_barang, 0, strpos($request->foto_barang, ';')))[1])[1];
            \Image::make($request->foto_barang)->save(public_path('img/items/').$foto);

            $items->foto_barang = $foto;
        }

        $file_invoice = $request->get('file_invoice');
        if($file_invoice){
            $invoice = time().'.' . explode('/', explode(':', substr($request->file_invoice, 0, strpos($request->file_invoice, ';')))[1])[1];
            \Image::make($request->file_invoice)->save(public_path('img/invoice-items/').$invoice);

            $items->file_invoice = $invoice;
        }
        $items->keterangan = $request->get('keterangan');
        $items->kategori_id = $request->get('kategori_id');
        $items->jenisbarang_id = $request->get('jenisbarang_id');
        
        $items->created_by = $user->id;
        $items->save();

        return['message'=> "Success"];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user =  auth('api')->user();

        $item = Barang::findorFail($id);

        $this->validate($request,[
            'nama_barang' => 'required|string|max:191',
            'foto_barang' => 'required',
            'file_invoice' => 'required'
        ]);


        $currentPhoto = $item->foto_barang;
        $foto_barang = $request->get('foto_barang');
        if($foto_barang != $currentPhoto){
            $nameFoto = time().'.' . explode('/', explode(':', substr($foto_barang, 0, strpos($foto_barang, ';')))[1])[1];
            \Image::make($foto_barang)->save(public_path('img/items/').$nameFoto);

            $request->merge(['foto_barang' => $nameFoto]);

            $itemPhoto = public_path('img/items/').$currentPhoto; 
            if(file_exists($itemPhoto)){
                @unlink($itemPhoto);
            }
        }

        $currentFile = $item->file_invoice;
        $file_invoice = $request->get('file_invoice');
        if($file_invoice != $currentFile){
            $nameFile = time().'.' . explode('/', explode(':', substr($file_invoice, 0, strpos($file_invoice, ';')))[1])[1];
            \Image::make($file_invoice)->save(public_path('img/invoice-items/').$nameFile);

            $request->merge(['file_invoice' => $nameFile]);

            $itemFile = public_path('img/invoice-items/').$currentFile; 
            if(file_exists($itemFile)){
                @unlink($itemFile);
            }
        }
        $item->kategori_id = $request->get('kategori_id');
        $item->jenisbarang_id = $request->get('jenisbarang_id');
        $item->updated_by = $user->id;
        $item->update($request->all());
        // return $item;
        return['message'=> "Success"];    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($kodebarang)
    {
        $this->authorize('isAdmin');
        $items = Barang::findOrFail($kodebarang);
        if($items->dipinjam == 0){
            $items->delete();
            return ['message' => ' Items Deleted'];
        }else{
            $error = \Illuminate\Validation\ValidationException::withMessages([
                'error' => ['Barang sedang dipinjam tidak dapat di hapus'],
            ]);
            throw $error;
        }
    }

    //file trashed
    public function trash(){
        if (\Gate::allows('isAdmin') || \ Gate::allows('isAuthor')){
            $deleted_items = DB::table('barang')->select('barang.kode_barang', 'barang.nama_barang', 'kategori_inventaris.kategori'
            , 'kategori_inventaris.id AS kategori_id', 'jenis_barang.jenis','jenis_barang.id as jenisbarang_id','barang.kelengkapan'
            ,'barang.foto_barang','barang.file_invoice','barang.status' ,'barang.merk','barang.keterangan', 'barang.dipinjam','barang.deleted_at',
             'users.name AS nama')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            )->leftJoin(
                'users',
                'users.id' , '=', 'barang.deleted_by'
            )->whereNotNull('deleted_at')->orderBy('barang.deleted_at','desc')->paginate(15);
            return $deleted_items;
        }
    }

    //restore items deleted
    public function restore($id){
        $items = Barang::withTrashed()->findOrFail($id);
        if($items->trashed()){
        $items->restore();
        } else {
            return ['message' => 'Items is not in trash'];
        }
        return ['message' => 'Items successfully restored'];
    }

    // delete permanent
    public function deletePermanent($id){
        $items = Barang::withTrashed()->findOrFail($id);
        if(!$items->trashed()){
            return ['message' => 'Can not delete permanent active items'];
        } else {
            $items->forceDelete();
            return ['message' => 'Items permanently deleted'];
        }
    }
    //find items
    public function search(){
        if ($search = \Request::get('q')){
           $items = DB::table('barang')->select('barang.kode_barang', 'barang.nama_barang', 'kategori_inventaris.kategori'
            , 'jenis_barang.jenis', 'barang.status' ,'barang.merk', 'barang.dipinjam','barang.created_at')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            )->leftJoin(
                'users',
                'users.id' , '=', 'barang.deleted_by'
            )->where(function($query) use ($search){
                $query->where('barang.nama_barang','LIKE', "%$search%");
            })->whereNull('barang.deleted_at')->orderBy('barang.created_at','desc')->paginate(15);
        }else{
            $items = DB::table('barang')->select('barang.kode_barang', 'barang.nama_barang', 'kategori_inventaris.kategori'
            , 'jenis_barang.jenis', 'barang.status' ,'barang.merk', 'barang.dipinjam','barang.created_at')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            )->leftJoin(
                'users',
                'users.id' , '=', 'barang.deleted_by'
            )->whereNull('barang.deleted_at')->orderBy('barang.deleted_at','desc')->paginate(15);
        }
        return $items;
    }

    //find items deleted
    public function searchDeleted(){
        if ($search = \Request::get('q')){
           $items = DB::table('barang')->select('barang.kode_barang', 'barang.nama_barang', 'kategori_inventaris.kategori'
            , 'jenis_barang.jenis', 'barang.status' ,'barang.merk', 'barang.deleted_at','users.name as nama')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            )->leftJoin(
                'users',
                'users.id' , '=', 'barang.deleted_by'
            )->where(function($query) use ($search){
                $query->where('barang.nama_barang','LIKE', "%$search%");
            })->whereNotNull('barang.deleted_at')->orderBy('barang.deleted_at','desc')->paginate(15);
        }else{
            $items = DB::table('barang')->select('barang.kode_barang', 'barang.nama_barang', 'kategori_inventaris.kategori'
            , 'jenis_barang.jenis', 'barang.status' ,'barang.merk', 'barang.deleted_at','users.name as nama')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            )->leftJoin(
                'users',
                'users.id' , '=', 'barang.deleted_by'
            )->whereNotNull('barang.deleted_at')->orderBy('barang.deleted_at','desc')->paginate(15);
        }
        return $items;
    }

    //find items Available
    public function searchAvailable(){
        if ($search = \Request::get('q')){
            $items = DB::table('barang')->select('barang.kode_barang', 'barang.nama_barang', 'kategori_inventaris.kategori'
            , 'jenis_barang.jenis', 'barang.status' ,'barang.merk', 'barang.created_at')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            )->leftJoin(
                'users',
                'users.id' , '=', 'barang.deleted_by'
            )->where('barang.status','=','Baik')
            ->where('barang.dipinjam','=',0)
            ->where(function($query) use ($search){
                $query->where('jenis_barang.jenis','LIKE', "%$search%");
            })
            ->whereNull('barang.deleted_at')->orderBy('barang.created_at','desc')->get();
        }else{
            $items = DB::table('barang')->select('barang.kode_barang', 'barang.nama_barang', 'kategori_inventaris.kategori'
            , 'jenis_barang.jenis', 'barang.status' ,'barang.merk', 'barang.created_at')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            )->leftJoin(
                'users',
                'users.id' , '=', 'barang.deleted_by'
            )->where('barang.status','=','Baik')
            ->where('barang.dipinjam','=',0)
            ->whereNull('barang.deleted_at')->orderBy('barang.deleted_at','desc')->get();
        }
        return $items;
    }


    //filter items
    public function filter($kategori, $jenis, $status){
        $items = DB::table('barang')->select('barang.kode_barang', 'barang.nama_barang', 'kategori_inventaris.kategori'
        , 'jenis_barang.jenis', 'barang.status' ,'barang.merk',  'barang.dipinjam','barang.created_at')
        ->leftJoin(
            'kategori_inventaris',
            'kategori_inventaris.id', '=', 'barang.kategori_id'
        )->leftJoin(
            'jenis_barang',
            'jenis_barang.id' , '=', 'barang.jenisbarang_id'
        )->when($kategori != 'all', function($items) use ($kategori){
            $items->where('kategori_inventaris.id', $kategori);
        })->when($jenis != 'all', function($items) use ($jenis){
            $items->where('jenis_barang.id', $jenis);
        })->when($status != 'all', function($items) use ($status){
            $items->where('barang.status', $status);
        })->whereNull('barang.deleted_at')
        ->orderBy('barang.created_at','desc')->paginate(15);

        return $items;
    }
    //filter items deleted
    public function filterDeleted($kategori, $jenis, $status){
        $items = DB::table('barang')->select('barang.kode_barang', 'barang.nama_barang', 'kategori_inventaris.kategori'
        , 'jenis_barang.jenis', 'barang.status' ,'barang.merk', 'barang.deleted_at','users.name as nama')
        ->leftJoin(
            'kategori_inventaris',
            'kategori_inventaris.id', '=', 'barang.kategori_id'
        )->leftJoin(
            'jenis_barang',
            'jenis_barang.id' , '=', 'barang.jenisbarang_id'
        )->leftJoin(
            'users',
            'users.id' , '=', 'barang.deleted_by'
        )->whereNotNull('barang.deleted_at')
        ->when($kategori != 'all', function($items) use ($kategori){
            $items->where('kategori_inventaris.id', $kategori);
        })->when($jenis != 'all', function($items) use ($jenis){
            $items->where('jenis_barang.id', $jenis);
        })->when($status != 'all', function($items) use ($status){
            $items->where('barang.status', $status);
        })
        ->orderBy('barang.deleted_at','desc')->paginate(15);

        return $items;
    }

    public function getDataExcel(){
        $items = DB::table('barang')->select('barang.kode_barang', 'barang.nama_barang','kategori_inventaris.kategori',
        'jenis_barang.jenis', 'barang.merk','barang.status','barang.kelengkapan', 'barang.keterangan')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            )->whereNull('deleted_at')->groupBy('kategori_inventaris.kategori')->groupBy('jenis_barang.jenis')
            ->orderBy('barang.created_at','desc')->get();
        return $items;
    }

    public function getDataBarangAvailable(){
        $items = DB::table('barang')->select('barang.kode_barang', 'barang.nama_barang', 'kategori_inventaris.kategori'
            , 'kategori_inventaris.id AS kategori_id', 'jenis_barang.jenis','jenis_barang.id as jenisbarang_id','barang.kelengkapan'
            ,'barang.foto_barang','barang.file_invoice','barang.status' ,'barang.merk','barang.keterangan', 'barang.created_at')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            )->where('status','=','Baik')->where('dipinjam','=',0)
            ->whereNull('deleted_at')->orderBy('barang.created_at','desc')->paginate(15);
            return $items;
    }

    public function getDataDashboard(){
        $items = Barang::count();
        $available = Barang::all()->where('dipinjam','=',0)->count();
        $rents = Barang::all()->where('dipinjam','=',1)->count();
        $brokeItems = Barang::all()->where('status', '=', 'Rusak')->count();
        $goodItems = Barang::all()->where('status','=','Baik')->count();
        $repaireItems = Barang::all()->where('status','=','Perbaikan')->count();
        $missItems = Barang::all()->where('status','=','Hilang')->count();
        $data_dashboardBarang = [
            'items'  => $items,
            'available'   => $available,
            'rents'  => $rents,
            'brokeItems'   => $brokeItems,
            'goodItems' => $goodItems,
            'repaireItems' => $repaireItems,
            'missItems' => $missItems,
        ];

        return $data_dashboardBarang;
    }
}
