<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\HeaderPeminjaman;
use App\DetailPeminjaman;
use App\Barang;
use Illuminate\Support\Facades\DB;

class PeminjamanBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        if (\Gate::allows('isAdmin') || \ Gate::allows('isAuthor')){
            $peminjaman = 
            DB::table('header_peminjaman_barang')->
            select('nomor_surat','tgl_surat', 'nama_proyek', 'keterangan','penanggungjawab.name as penanggungjawab','peminjam.name as peminjam',
            'admin.name as admin')
                ->join(
                'proyek',
                'header_peminjaman_barang.proyek_id', '=', 'proyek.id'
                )->join(
                    'users as penanggungjawab',
                    'header_peminjaman_barang.penanggung_jawab', '=', 'penanggungjawab.id'
                )->join(
                    'users as peminjam',
                    'header_peminjaman_barang.peminjam', '=', 'peminjam.id'
                )->join(
                    'users as admin',
                    'header_peminjaman_barang.created_by', '=', 'admin.id'
                )->orderBy('header_peminjaman_barang.created_at','desc')->paginate(15);
            
            return $peminjaman;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user =  auth('api')->user();
        $rules = [
            'nomor_surat' => 'required|string|max:191',
            'tgl_pinjam' => 'required|string|max:191',
            'id_proyek' => 'required',
            'iduser_penanggungjawab' => 'required',
            'iduser_peminjam' => 'required',
            'keterangan'=> 'required',
            'barang'=>'required'
        ];
    
        $customMessages = [
            'barang.required' => 'Barang Harus Dipilih'
        ];
    
        $this->validate($request, $rules, $customMessages);
        
        $proyek = $request->get('id_proyek')['id'];
        $penanggung_jawab = $request->get('iduser_penanggungjawab')['id'];
        $peminjam = $request->get('iduser_peminjam')['id'];
        $tgl_pinjam = $request->get('tgl_pinjam');
        $keterangan = $request->get('keterangan');
        $barang = $request->get('barang');

        $header_peminjaman = new HeaderPeminjaman;
        $header_peminjaman->nomor_surat = $request->get('nomor_surat');
        $header_peminjaman->tgl_surat = $tgl_pinjam;
        $header_peminjaman->proyek_id = $proyek;
        $header_peminjaman->keterangan = $keterangan;
        $header_peminjaman->penanggung_jawab = $penanggung_jawab;
        $header_peminjaman->peminjam = $peminjam;
        $header_peminjaman->aktif = 1;
        $header_peminjaman->created_by = $user->id;
        $header_peminjaman->save();

        $no =0;
        $item = (array) $barang;
        foreach ($item as $barang_pinjam){
            $detail_peminjaman = new DetailPeminjaman;    
            $detail_peminjaman->nomor_surat = $request->get('nomor_surat');
            $detail_peminjaman->kode_barang = $item{$no};
            $detail_peminjaman->created_by = $user->id;
            $detail_peminjaman->save();
            $barang_dipinjam = Barang::findorFail($item{$no});
            $barang_dipinjam->dipinjam = 1;
            $barang_dipinjam->update();
            $no++;
        }
        return ['message'=> "Success"];

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function search(){
        if ($search = \Request::get('q')){
           $items = DB::table('header_peminjaman_barang')->
           select('nomor_surat','tgl_surat', 'nama_proyek', 'keterangan','penanggungjawab.name as penanggungjawab','peminjam.name as peminjam',
           'admin.name as admin')
               ->join(
               'proyek',
               'header_peminjaman_barang.proyek_id', '=', 'proyek.id'
               )->join(
                   'users as penanggungjawab',
                   'header_peminjaman_barang.penanggung_jawab', '=', 'penanggungjawab.id'
               )->join(
                   'users as peminjam',
                   'header_peminjaman_barang.peminjam', '=', 'peminjam.id'
               )->join(
                   'users as admin',
                   'header_peminjaman_barang.created_by', '=', 'admin.id'
               )->where(function($query) use ($search){
                $query->where('header_peminjaman_barang.nomor_surat','LIKE', "%$search%");
                })->orderBy('header_peminjaman_barang.created_at','desc')->paginate(15);
        }else{
            $items =  DB::table('header_peminjaman_barang')->
            select('nomor_surat','tgl_surat', 'nama_proyek', 'keterangan','penanggungjawab.name as penanggungjawab','peminjam.name as peminjam',
            'admin.name as admin')
            ->join(
            'proyek',
            'header_peminjaman_barang.proyek_id', '=', 'proyek.id'
            )->join(
                'users as penanggungjawab',
                'header_peminjaman_barang.penanggung_jawab', '=', 'penanggungjawab.id'
            )->join(
                'users as peminjam',
                'header_peminjaman_barang.peminjam', '=', 'peminjam.id'
            )->join(
                'users as admin',
                'header_peminjaman_barang.created_by', '=', 'admin.id'
            )->orderBy('header_peminjaman_barang.deleted_at','desc')->paginate(15);
        }
        return $items;
    }
    
    public function getDataPeminjaman($id){
        $header_peminjaman = DB::table('header_peminjaman_barang')->
        select('nomor_surat','tgl_surat', 'nama_proyek', 'keterangan','penanggungjawab.name as penanggungjawab','peminjam.name as peminjam',
         'admin.name as admin')
            ->join(
            'proyek',
            'header_peminjaman_barang.proyek_id', '=', 'proyek.id'
            )->join(
                'users as penanggungjawab',
                'header_peminjaman_barang.penanggung_jawab', '=', 'penanggungjawab.id'
            )->join(
                'users as peminjam',
                'header_peminjaman_barang.peminjam', '=', 'peminjam.id'
            )->join(
                'users as admin',
                'header_peminjaman_barang.created_by', '=', 'admin.id'
            )->where('nomor_surat','=',$id)->get();


        $items = DB::table('barang')->select('kode_barang', 'nama_barang', 'merk', 'kelengkapan', 'kategori', 'jenis')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            );
        $detail_peminjaman = DB::table('detail_peminjaman_barang')
        ->joinSub($items, 'items', function ($join) {
            $join->on('detail_peminjaman_barang.kode_barang', '=', 'items.kode_barang');
        })->where('nomor_surat','=',$id)->get();

        $data_peminjaman = [
            'header_peminjaman'  => $header_peminjaman,
            'detail_peminjaman'   => $detail_peminjaman
        ];
        return $data_peminjaman;
    }

    public function getDetailPeminjaman($id){
        $items = DB::table('barang')->select('kode_barang', 'nama_barang', 'merk', 'kelengkapan', 'kategori', 'jenis', 'foto_barang')
            ->leftJoin(
                'kategori_inventaris',
                'kategori_inventaris.id', '=', 'barang.kategori_id'
            )->leftJoin(
                'jenis_barang',
                'jenis_barang.id' , '=', 'barang.jenisbarang_id'
            );
        $detail_peminjaman = DB::table('detail_peminjaman_barang')
        ->joinSub($items, 'items', function ($join) {
            $join->on('detail_peminjaman_barang.kode_barang', '=', 'items.kode_barang');
        })->where('detail_peminjaman_barang.nomor_surat','=',$id)->get();
        return $detail_peminjaman;
    }
}
