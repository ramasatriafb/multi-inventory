<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = 'kategori_inventaris';

    protected $fillable = [
        'kategori'
    ];

    public function items(){
        return $this->hasMany('App\Barang');
    }
}
