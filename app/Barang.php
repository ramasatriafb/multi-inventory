<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Barang extends Model
{
    use SoftDeletes;
    protected $table = 'barang';
    protected $fillable = ['kode_barang','nama_barang','merk','kelengkapan','status','foto_barang','file_invoice',
    'keterangan','updated_by','deleted_by','kategori_id', 'jenisbarang_id'];
    protected $primaryKey = 'kode_barang';

    public function kategori(){
        return $this->belongsTo('App\Kategori');
    }

    public function jenisbarang(){
        return $this->belongsTo('App\JenisBarang');
    }

    public function peminjaman(){
        return $this->belongsToMany("App\DetailPeminjaman");
    }
}
