<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HeaderPeminjaman extends Model
{
    protected $table = 'header_peminjaman_barang';
    protected $fillable = ['nomor_surat','tgl_surat','proyek_id','keterangan','penanggung_jawab','peminjam','aktif',
    'updated_by','deleted_by'];
    protected $primaryKey = 'nomor_surat';

    public function proyek(){
        return $this->belongsTo('App\Proyek');
    }
}
