<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisBarang extends Model
{
    protected $table = 'jenis_barang';

    protected $fillable = [
        'jenis'
    ];

    public function items(){
        return $this->hasMany('App\Barang');
    }
}
