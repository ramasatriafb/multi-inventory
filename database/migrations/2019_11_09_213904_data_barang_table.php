<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataBarangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barang', function(Blueprint $table){
            $table->string('kode_barang')->unique()->primary();
            $table->integer('kategori_id')->unsigned();
            $table->integer('jenisbarang_id')->unsigned();
            $table->string('nama_barang');
            $table->string('merk')->nullable();;
            $table->mediumText('kelengkapan')->nullable();
            $table->string('status');
            $table->string('foto_barang')->default('items.png');
            $table->string('file_invoice');
            $table->mediumText('keterangan')->nullable();
            $table->integer('dipinjam')->default(0);
            $table->bigInteger('created_by')->unsigned();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('kategory_id')->references('id')->on('kategori_inventaris')->onDelete('restrict');
            $table->foreign('jenisbarang_id')->references('id')->on('jenis_barang')->onDelete('restrict');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barang');
    }
}
