<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResources(['user' => 'API\UserController']);
Route::get('profile', 'API\UserController@profile');
Route::put('profile', 'API\UserController@updateProfile');
Route::get('findUser', 'API\UserController@search');
Route::get('getUser', 'API\UserController@getUser');

Route::apiResources(['kategori' => 'API\KategoriController']);
Route::get('findKategori', 'API\KategoriController@search');
Route::get('getDataKategoriInventaris', 'API\KategoriController@getDataKategoriInventaris');

Route::apiResources(['jenis' => 'API\JenisBarangController']);
Route::get('findJenis', 'API\JenisBarangController@search');
Route::get('getDataJenisBarang', 'API\JenisBarangController@getDataJenisBarang');

Route::apiResources(['proyek' => 'API\ProyekController']);
Route::get('findProyek', 'API\ProyekController@search');
Route::get('getProyek', 'API\ProyekController@getProyek');


Route::apiResources(['barang' => 'API\BarangController']);
Route::get('findBarang', 'API\BarangController@search');
Route::get('filterBarang/{kategori}/{jenis}/{status}', 'API\BarangController@filter');


Route::get('findBarangHapus', 'API\BarangController@searchDeleted');
Route::get('filterBarangHapus/{kategori}/{jenis}/{status}', 'API\BarangController@filterDeleted');
Route::get('barang-dihapus', 'API\BarangController@trash');
Route::delete('hapusPermanent/{id}','API\BarangController@deletePermanent');
Route::get('/barangdirestore/{id}', 'API\BarangController@restore');
Route::get('/getdataExcel', 'API\BarangController@getDataExcel');
Route::get('/getDataBarangAvailable', 'API\BarangController@getDataBarangAvailable');
Route::get('findBarangAvailable', 'API\BarangController@searchAvailable');

Route::get('dashboardBarang', 'API\BarangController@getDataDashboard');

Route::apiResources(['peminjamanbarang' => 'API\PeminjamanBarangController']);
Route::get('getDataPeminjaman/{id}', 'API\PeminjamanBarangController@getDataPeminjaman');
Route::get('findPeminjaman', 'API\PeminjamanBarangController@search');
Route::get('getDetailPeminjaman/{id}', 'API\PeminjamanBarangController@getDetailPeminjaman');